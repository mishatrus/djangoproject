from django.contrib import admin
from app.models import Product, CreditApp, Contract, Producer


# Register your models here.

@admin.register(CreditApp)
class CreditAppAdmin(admin.ModelAdmin):
    pass


@admin.register(Contract)
class ContractAdmin(admin.ModelAdmin):
    pass


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass


@admin.register(Producer)
class ProducerAdmin(admin.ModelAdmin):
    pass
