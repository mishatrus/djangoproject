from django.db import models
from django.db import connection, reset_queries
from django.db.models.query import QuerySet
import time
import functools


def query_debugger(func):
    @functools.wraps(func)
    def inner_func(*args, **kwargs):
        reset_queries()

        start_queries = len(connection.queries)

        start = time.perf_counter()
        result = func(*args, **kwargs)
        end = time.perf_counter()

        end_queries = len(connection.queries)

        print(f"Количетсво запросов : {end_queries - start_queries}")
        print(f"Время выполения : {(end - start):.2f}s")
        return result

    return inner_func


# Create your models here.
class AbsModel(models.Model):
    class Meta:
        abstract = True

    date_creation = models.DateTimeField(verbose_name='Дата и время создания', auto_now_add=True)
    date_updated = models.DateTimeField(verbose_name='Дата и время последнего изменения', auto_now=True)


class Producer(AbsModel):
    name = models.CharField(max_length=30, verbose_name='Наименование производителя', unique=True)

    def __str__(self):
        return f"{self.name}"


class Product(AbsModel):
    name = models.CharField(max_length=30, verbose_name='Наименование товара')
    producer = models.ForeignKey(Producer, on_delete=models.PROTECT, verbose_name='Производитель')

    class Meta:
        unique_together = ('name', 'producer',)

    def __str__(self):
        return f"{self.name}, {self.producer}"


class CreditApp(AbsModel):
    fio = models.CharField(max_length=25, verbose_name='ФИО заявителя')
    products = models.ManyToManyField(Product, verbose_name='Продукты')

    def __str__(self):
        return f"Кредитная заявка №{self.pk}"

    @property
    def get_unique_producer_ids(self):
        return self.products.prefetch_related('producer').values_list('producer_id', flat=True).distinct()


class Contract(AbsModel):
    credit_app = models.OneToOneField(CreditApp, on_delete=models.PROTECT,
                                      verbose_name='Кредитная заявка')

    def __str__(self):
        return f"Контракт №{self.pk} на {self.credit_app}"

    @property
    def credit_name(self):
        return f"Контракт №{self.pk}"


@query_debugger
def get_producers_ids(*, contract_id=None) -> None:
    if contract_id:
        qs = CreditApp.objects.get(contract=contract_id).products.values_list('producer__id', flat=True).distinct()
        print(qs)
